call plug#begin('~/local/share/nvim/plugged')

Plug 'https://github.com/itchyny/lightline.vim'
Plug 'morhetz/gruvbox'
Plug 'chriskempson/base16-vim'

call plug#end()

syntax enable
set number
set relativenumber
set clipboard=unnamedplus
colorscheme base16-atelier-forest
let base16colorspace=256
set termguicolors

