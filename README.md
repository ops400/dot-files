# Dot Files
A reformed version of mine previous repo of my config files.
## Dependencies
### NeoVim's
[vim-plug](https://github.com/junegunn/vim-plug)<br>
Make sure to get the NeoVim version.
### ArchLinux
```freetype2```<br>
```libx11```<br>
```libxft```<br>
```libxinerama```<br>
```fontconfig```<br>
```coreutils```<br>
```glibc```<br>
```base-devel```<br>
```xorg-xinit```<br>
```xclip```<br>
```neovim``` <br>
```nitrogen``` <br>
```xorg``` <br>
```hyprland-git``` <br>
```hyprpaper``` <br>
```tofi```(AUR)<br>
```waybar-hyprlan-git``` <br>
```alacritty``` <br>
```pfetch```(AUR) <br>
```ttf-ubuntu-mono-nerd``` <br>
```otf-font-awesome``` <br>
```ttf-font-awesome``` <br> 
```pacman-contrib``` <br>
```xdg-desktop-portal-hyprland```
## Configuration
### The "xinitstuff"
Enter the ```xinitstuff``` directory, rename the ```xinitrc``` file to ```.xinitrc``` and copy it to home (```~/```).
### The "bashstuff"
Enter the ```bashstuff``` directory, rename ```bashrc``` to ```.bashrc```, ```bash_profile``` to ```.bash_profile``` and copy the them to your home directory (```~/```).
### dmenu
Enter the ```dmenu``` directory and run ```sudo make clean install``` or ```doas make clean install```.
### gtk-3.0
Copy the ```gtk-3.0``` directory to ```~/.config``` (your config folder).
### dwmblocks
If you want a battery display enter the ```dwmblockslaptop``` directory, if not enter the ```dwmblocks``` directory, either case run ```sudo make clean install``` or ```doas make clean install```.
### dwm
Enter the ```dwm``` directory and run run ```sudo make clean install``` or ```doas make clean install```.
### st
Enter the ```st``` directory and run run ```sudo make clean install``` or ```doas make clean install```.
### neovim
Copy the ```nvim``` directory to your config folder (```~/.config```), the enter nvim and run ```:PlugInstall```.
## At this point I give up trying to make installation instructions.
