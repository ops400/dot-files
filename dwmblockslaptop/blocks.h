//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
//	{"UTDs:", "sudo xbps-install -Sun | wc -l",	0,	0},
	{"PKGs:", "xbps-query -l | wc -l", 	0,	0},
	{"BAT:", "cat /sys/class/power_supply/BAT1/capacity", 30, 0},
	{"RAM:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",	30,		0},
//	{"CPU:", "mpstat | awk '$12 ~ /[0-9.]+/ { print 100 - $12\"%\" }'",	20,	0},
//	{"Kernel: ", "uname -r",		0,	0},
	{"", "date '+%d-%m-%y %I:%M'",					30,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
